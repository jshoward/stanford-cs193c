/*
	CS193C Class Project; Crossword Puzzle
    John Sergent Howard
    jshoward@Stanford.EDU
*/
/*
   crossword puzzle from New York Times, 11/13/2002 free
   puzzle archive
*/
var solutionString = "";
var gridPattern = "";
var currentState = "";
var clues = null;
var across = "across";
var down = "down";
var puzzleHeaderLength = 52;
var puzzleWidth = 15; // puzzleString[44];
var puzzleLength = 15; // puzzleString[45];
var puzzleSize = puzzleWidth * puzzleLength;

//
// Generate the crossword grid and contents based
// on the specified gridPattern. Returns a string
// containing the XHTML code representing gridPattern.
//
function generateGrid( gridPattern ) {
	var clueIndex = 0;
	var clueCounter = 1;
	var deadLeft = false;
	var deadAbove = false;
	var onLeftEdge = false;
	var tableStart = '<table class="grid">';
	var tableEnd = '</table>';
	var tr = '<tr>';
	var trEnd = '</tr>';
	var tdStart = '<td>';
	var tdEnd = '</td>';
	var deadCell = '<td class="dead">&nbsp;</td>';
	var supStart = '<sup>';
	var supEnd = '&nbsp;</sup>';
	var generatedHTML = tableStart;

	for( var i = 0; i < gridPattern.length; i++ ) {
		if( i % puzzleWidth == 0 ) {
			onLeftEdge = true;
			generatedHTML += tr;
			tr = '</tr><tr>';
		} else {
			onLeftEdge = false;
		}
		if( gridPattern[i] == "." ) {
			generatedHTML += deadCell;
		} else {
			generatedHTML += tdStart;
			if( onLeftEdge ) {
				deadLeft = true;
			} else {
				deadLeft = ( gridPattern[i-1] == "." )
							? true
							: false;
			}
			if( (i < puzzleWidth) ) {
				deadAbove = true;
			} else {
				deadAbove = ( gridPattern[i-puzzleWidth] == "." )
							? true
							: false;
			}
			if( deadLeft || deadAbove ) {
				generatedHTML += supStart + clueCounter + supEnd;
				if( deadLeft ) {
					clues[clueIndex].num = clueCounter;
					clues[clueIndex].patternIndex = i;
					clues[clueIndex++].direction = across;
				}
				if( deadAbove ) {
					clues[clueIndex].num = clueCounter;
					clues[clueIndex].patternIndex = i;
					clues[clueIndex++].direction = down;
				}
				clueCounter++;
			}
			generatedHTML += ( gridPattern[i] == "-" )
							? "&nbsp;"
							: gridPattern[i];
			generatedHTML += tdEnd;
		}
	}
	generatedHTML += trEnd + tableEnd;
	return generatedHTML;
}

//
// Generate the crossword clues based on the specified clues.
// Returns a string containing the XHTML code representing the
// clues in 2 columns. Note that the columns are balanced
// regardless of whether there are more "Across" or "Down" clues.
//
function generateClues( clues ) {
	var cluesHTML = '<form id="enterAnswer" action=""><table class="clues"><tr><td colspan="2" class="clues">Select a clue and enter answer:<input type="text" name="answer" size="15" /><input type="button" value="enter" onclick="enterWord();" /></td></tr></table></form><form id="clueSelector" action=""><table class="clues"><tr><td class="clues">Across</td><td class="clues">Down</td></tr>';
	var cluesTrailer = '</table></form>';
	var cluesEntryStart = '<td class="clues"><input type="radio" name="clueRadio" value="';
	var cluesEntryEnd = '" />';
	var spacer = '<td class="clues">&nbsp;</td>';
	var tdEnd = '</td>';
	var dotNBSP = ".&nbsp;";
	var acrossHTML = new Array();
	var downHTML = new Array();
	var acrossIndex = 0;
	var downIndex = 0;
	var smallerLength = 0;
	var acrossLonger = false;

	for( var i = 0; i < clues.length; i++ ) {
		if( clues[i].direction == across ) {
			acrossHTML[acrossIndex++] = cluesEntryStart
						+ clues[i].num + "|" + clues[i].direction
						+ cluesEntryEnd + clues[i].num + dotNBSP
						+ clues[i].text + tdEnd;
		} else if( clues[i].direction == down ) {
			downHTML[downIndex++] = cluesEntryStart
						+ clues[i].num + "|" + clues[i].direction
						+ cluesEntryEnd + clues[i].num + dotNBSP
						+ clues[i].text + tdEnd;
		}
	}
	if( acrossHTML.length > downHTML.length ) {
		listLength = acrossHTML.length;
		smallerLength = downHTML.length;
		acrossLonger = true;
	} else {
		listLength = downHTML.length;
		smallerLength = acrossHTML.length;
		acrossLonger = false;
	}
	for( var i = smallerLength; i < listLength; i++ ) {
		if( acrossLonger ) {
			downHTML[i] = spacer;
		} else {
			acrossHTML[i] = spacer;
		}
	}
	for( var i = 0; i < listLength; i++ ) {
		cluesHTML += '<tr class="clues">'
					+ acrossHTML[i] + downHTML[i]
					+ '</tr>';
	}
	cluesHTML += cluesTrailer;
	return cluesHTML;
}

//
// Returns an index in the specified pattern where the
// targetNum clue solution, going in direction (across or
// down), should be located
//
function findInsertPoint( pattern, targetNum, direction ) {
	for( var i = 0; i < pattern.length; i++ ) {
		if( (clues[i].direction == direction)
			&& (clues[i].num == targetNum) ) {
			return clues[i].patternIndex;
		}
	}
	return -1;
}

//
// gets a word from the input area and inserts it into
// the current puzzle state.
//
function enterWord() {
	var sep = -1;
	var insert = 0;
	var insertBeneath = 0;
	var maxChadLen = 0;
	var front = "";
	var rear = "";
	var chad = "";
	var currRow = "";
	var target = new Object();
    var answerForm =
                document.getElementById( "enterAnswer" );
    var clueRadios = document.getElementById( "clueSelector" ).elements;
    var gridElem = document.getElementById( "xword" );

    target.text = answerForm.answer.value;
    answerForm.answer.value = "";
	for( var i = 0; i < clueRadios.length; i++ ) {
	    if( clueRadios[i].checked ) {
	    	sep = clueRadios[i].value.indexOf("|");
	    	if( sep == -1 ) {
	    		// this should never happen
	    		return;
	    	}
	    	target.num = clueRadios[i].value.substring( 0, sep );
	    	target.direction = clueRadios[i].value.substring( sep+1 );
            break;
        }
    }
    if( target.direction == across ) {
    	insert = findInsertPoint( currentState, target.num, across );
    	if( insert == -1 ) {
	    	// this should never happen
	    	return;
	    }
	    front = currentState.substring( 0, insert );
	    rear = currentState.substring( insert+target.text.length );
	    currRow = currentState.substring(insert,
							insert+(puzzleWidth-(insert%puzzleWidth)) );
	    maxChadLen = currRow.indexOf(".");
	    if( maxChadLen == -1 ) {
	    	maxChadLen = puzzleWidth-(insert%puzzleWidth);
	    }
	    chad = currentState.substring( insert,
	    								insert+maxChadLen );
	    if( chad.length > target.text.length ) {
	    	alert("The word you supplied does not have enough letters");
	    	return;
	    } else if( chad.length < target.text.length ) {
	    	alert("The word you supplied has too many letters");
	    	return;
	    }
	    currentState = front + target.text.toUpperCase() + rear;
	    gridElem.innerHTML = generateGrid( currentState );
    } else if( target.direction == down ) {
    	insert = findInsertPoint( currentState, target.num, down );
    	if( insert == -1 ) {
	    	// this should never happen
	    	return;
	    }
		for( var i = 0; i < target.text.length; i++ ) {
			insertBeneath = insert+(i*15);
			if( currentState[insertBeneath] != "." ) {
	    		front = currentState.substring( 0, insertBeneath );
	    		rear = currentState.substring( insertBeneath+1 );
	    		chad = currentState.substring( insertBeneath,
												insertBeneath+1 );
				currentState = front
						+ target.text.substring(i, i+1).toUpperCase()
						+ rear;
			} else {
				alert("The word you supplied has an incorrect number of letters");
				return;
			}
		}
    	gridElem.innerHTML = generateGrid( currentState );
    }
    return;
}

//
// Display the puzzle solution
//
function solution() {
    var divElem = document.getElementById( "xword" );

    divElem.innerHTML = generateGrid( solutionString );
    return;
}

//
// Clear the puzzle grid
//
function clearPuzzle() {
    var answerForm =
                document.getElementById( "enterAnswer" );
    var gridElem = document.getElementById( "xword" );

    answerForm.answer.value = ""; // clear the input text box
    gridElem.innerHTML = generateGrid( gridPattern ); // clear the grid
}

//
// Initialize the data structures.
// Note that these initializations are hard coded
// from the Nov1302/puz file. This initialization should
// be done by file access on the server.
//
function init() {
    clues = new Array(
    			{text: "Fare-beater's aid", num: 0,
    			 patternIndex: 0, direction: ""},
    			{text: "Urban ___ ", num: 0, patternIndex: 0,
    			 direction: ""},
    			{text: "New Jersey town near Teaneck", num: 0,
				 patternIndex: 0, direction: ""},
    			{text: "Anchorman's network?", num: 0, patternIndex: 0,
    			 direction: ""},
				{text: "Flip", num: 0, patternIndex: 0, direction: ""},
				{text: "Broadway dance hit of 1999", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Mountaineer's worry", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "This and that ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Belt out ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: '"Later!"', num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Store, as corn", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Soup bean", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Topper", num: 0, patternIndex: 0,
				 direction: ""},
				{text:
				 'Spanish queen who was given the title "the Catholic"',
				  num: 0, patternIndex: 0, direction: ""},
				{text: '"Congratulations!" ', num: 0, patternIndex: 0,
				 direction: ""},
				{text: "One with duties ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Canc&uacute;n coin ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Ufologist's study ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: '"This ___ good as it gets!"', num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Author Jaffe ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Prompters' utterances", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "H&auml;agen-___ ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Start of a quip ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Port of old Rome", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Part of a jazz combo", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "One who's always buzzing off?", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Moli&egrave;re's The School for ___ ", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Like", num: 0, patternIndex: 0, direction: ""},
				{text: "Loop trains", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Nonclerical", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "They may be running", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Qty. ", num: 0, patternIndex: 0, direction: ""},
				{text: "Autobahn sights", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Old anti-Communist assn.", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Glorifies", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Sounds of relief", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Like some car headlights ", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Where heroes may be found", num: 0,
				 patternIndex: 0, direction: ""},
				{text: " Not natural ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Succulent plant", num: 0, patternIndex: 0,
				 direction: ""},
				{text: " Middle of the quip ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "1949 Tracy/Hepburn film ", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Arrive", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Curiosity", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Kind of time", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "60's-70's gallery hangings", num: 0,
				 patternIndex: 0, direction: ""},
				{text: " Do business", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Brews", num: 0, patternIndex: 0, direction: ""},
				{text: "In danger", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Drinker", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Wizard, in slang", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Old-fashioned theaters", num: 0,
				 patternIndex: 0, direction: ""},
				{text: " Wee, to Burns ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Clog", num: 0, patternIndex: 0, direction: ""},
				{text: 'Oscar winner for "West Side Story"',
				 num: 0, patternIndex: 0, direction: ""},
				{text: "Dedicated", num: 0, patternIndex: 0,
				 direction: ""},
				{text: " N.Y.C. subway overseer", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Assn. formed in Bogot&aacute;", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Cut", num: 0, patternIndex: 0, direction: ""},
				{text: "Beanie's top, perhaps ", num: 0,
				 patternIndex: 0, direction: ""},
				{text: 'Jesse known as the "Buckeye Bullet"',
				 num: 0, patternIndex: 0, direction: ""},
				{text: "___ Na Na", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "End of the quip ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Come-on ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Actor Robert or Alan ", num: 0, direction: ""},
				{text: "Lip", num: 0, patternIndex: 0, direction: ""},
				{text: "Guns", num: 0, patternIndex: 0, direction: ""},
				{text: " Heathrow plane", num: 0, patternIndex: 0,
				 direction: ""},
				{text: " Throat doctor's concern", num: 0,
				 patternIndex: 0, direction: ""},
				{text: " There's little point to it ", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Subject for Monet", num: 0, patternIndex: 0,
				 direction: ""},
				{text: " Pocket protector brigade", num: 0,
				 patternIndex: 0, direction: ""},
				{text: " Former Georgia senator", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Sunday ___ ", num: 0, patternIndex: 0,
				 direction: ""},
				{text: "Some of them are gray", num: 0,
				 patternIndex: 0, direction: ""},
				{text: "Smudge", num: 0, patternIndex: 0, direction: ""}
			);
	solutionString =
"SLUG.FOSSE.LIMAPESO.ALIEN.ISASRONA.LINES.DAZSANAPOLOGYIS.BEEWIVES...ALA.ELSLAY.TABS.EXALTS...DIMMED..ALOEAGREATWAYTOHAVEDEAL..STEEPS...ATRISK.ODEA.SMAMTA.OAS...ROTORSHA.THELASTWORDREVS.UVULA.EPEEIRIS.NERDS.NUNNBEST.AREAS.SPOT";
	gridPattern =
"----.-----.--------.-----.--------.-----.---------------.--------...---.------.----.------...------..-----------------------..------...------.----.------.---...--------.---------------.-----.--------.-----.--------.-----.----";
	currentState = gridPattern;

	return;
}

//
// The main function and starting point, this calls any
// necessary initialization and generates the initial grid
// and clue listing.
//
function xword() {
    var gridElem = document.getElementById( "xword" );
    var cluesElem = document.getElementById( "clues" );
    
    init();

    gridElem.innerHTML = generateGrid( gridPattern );
    cluesElem.innerHTML = generateClues( clues );
    return;
}